import "package:flutter/material.dart";

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  Map data = {};

  @override
  Widget build(BuildContext context) {
    data = data.isNotEmpty ? data : ModalRoute.of(context).settings.arguments;
    String backgroundImage =
        data["isDaytime"] ? "assets/day.png" : "assets/night.jpg";

    Color backgroundColor = data["isDaytime"] ? Colors.blue : Colors.indigo[700];

    return Scaffold(
      backgroundColor: backgroundColor,
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage(backgroundImage),
            fit: BoxFit.cover,
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.fromLTRB(0, 120, 0, 0),
          child: SafeArea(
            child: Column(
              children: <Widget>[
                FlatButton.icon(
                  onPressed: () async{
                   dynamic result = await Navigator.pushNamed(context, "/location");
                   setState(() {
                     data = {
                       "time":result["time"],
                       "location":result["location"],
                       "isDaytime":result["isDaytime"],
                       "flag":result["flag"],
                     };
                   });
                  },
                  icon: Icon(Icons.edit_location,color: Colors.white ,),
                  label: Text("Edit Location",style: TextStyle(
                    color: Colors.white
                  ),),
                ),
                SizedBox(height: 20),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      data["location"],
                      style: TextStyle(
                          fontSize: 28, color: Colors.white, letterSpacing: 2),
                    )
                  ],
                ),
                SizedBox(height: 20),
                Text(
                  data["time"],
                  style: TextStyle(fontSize: 66, color: Colors.white),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
